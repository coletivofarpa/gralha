# gralha.cc

Gralha.cc é encurtador de links baseado em confiança, o que garante a gestão compartilhada dos links entre as pessoas e coletivas que utilizam a ferramenta.

Este projeto é mantido por pessoas voluntárias, se você tiver interesse em participar ou contribuir, por favor escreva para [coletivofarpa@pm.me](mailto:coletivofarpa@pm.me).
